
public interface List {

	void add(String s);
	
	boolean removeAt(int i);
	
	boolean removeString(String s);
	
	String get(int i);
	
	int size();
	
	
	
}
