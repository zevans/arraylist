
public class ArrayList implements List {

	private String[]myArray;
	private int nextInsertion=0;
	
	public ArrayList(){
		myArray=new String [1];
	}
	
	@Override
	public void add(String s) {
		if(nextInsertion>=myArray.length){
			//new array
			String[] newArray=new String[myArray.length+1];
			//copy over
			for (int i = 0; i < myArray.length; i++) {
				newArray[i]=myArray[i];
			}
			myArray=newArray;
			
		}
		myArray[nextInsertion]=s;
		nextInsertion++;
		
	}

	@Override
	public boolean removeAt(int i) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean removeString(String s) {
		// TODO Auto-generated method stub
		for (int i = 0; i < nextInsertion; i++) {
			if(myArray[i]==s){//this works because of the java optimizer lol
				myArray[i]=null;
				
				for (int j = i; j < nextInsertion-1; j++) {
					
					myArray[j]=myArray[j+1];
				}
				
				nextInsertion--;
				return true;
			}
		}
		
		return false;
	}

	@Override
	public String get(int i) {
		// TODO Auto-generated method stub
		return myArray[i];
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return nextInsertion;
	}

}
